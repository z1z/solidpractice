/**
 * This class for econom-class cars (плацкарт).
 */
package kz.aitu.oop.practice.practice2.domain.carTypes;

import kz.aitu.oop.practice.practice2.domain.Car;
import kz.aitu.oop.practice.practice2.domain.Color;

public class EconomClassCar extends Car {

    public EconomClassCar(){
        super();
    }

    public EconomClassCar(int passengers){
        super(passengers);
    }

    public EconomClassCar(int passengers, Color color){
        super(passengers, color);
    }

    @Override
    public void info() {
        System.out.println("It is a econom-class car. It is designed for "+getPassengers()+" passengers.");
    }
}
