/**
 * Train class.
 * It always has locomotive.
 * It implements Trailer and Info interfaces.
 */
package kz.aitu.oop.practice.practice2.domain;

import kz.aitu.oop.practice.practice2.interfaces.Info;
import kz.aitu.oop.practice.practice2.interfaces.Trailer;

import java.util.LinkedList;
import java.util.List;


public class Train implements Trailer, Info {

    private Locomotive locomotive;  // locomotive
    private List<Car> cars;         // list of cars
    private int possibleSpeed;      // possible speed of train

    public Train(Locomotive locomotive) {
        setLocomotive(locomotive);
        cars = new LinkedList<>();
        setPossibleSpeed(locomotive.getMaxSpeed());
    }

    public Locomotive getLocomotive() {
        return locomotive;
    }

    public void setLocomotive(Locomotive locomotive) {
        this.locomotive = locomotive;
    }

    public List<Car> getCars() {
        return cars;
    }

    // get car on certain position. Here position = index in list + 1
    public Car getCar(int position) {
        return cars.get(position - 1);
    }

    public int getPossibleSpeed() {
        return possibleSpeed;
    }

    public void setPossibleSpeed(int possibleSpeed) {
        this.possibleSpeed = possibleSpeed;
    }

    // this function returns how many passengers could be in the train
    public int getTotalPassengers() {
        int total = 0;
        for (Car car : cars) {
            total += car.getPassengers();
        }
        return total;
    }

    public void drive() {
        locomotive.drive();
        System.out.print("And it pulls cars.");
    }

    // add car to the train. When it happens, train loses possible speed, because of security.
    @Override
    public void hitch(Car car) {
        cars.add(car);
        setPossibleSpeed((getPossibleSpeed() - (int) (car.getPassengers() * 0.1)));
        System.out.println("New car has been hitched.");
    }

    // remove car from the train. When it happens, train raises possible speed.
    @Override
    public void uncouple(int position) {
        setPossibleSpeed((getPossibleSpeed() + (int) (getCar(position).getPassengers() * 0.1)));
        cars.remove(position - 1);
        System.out.println("The car at position " + position + " has been removed.");
    }

    @Override
    public void info() {
        System.out.println("This is a train which contains " + cars.size() + " cars.");
    }
}
