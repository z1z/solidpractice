/**
 * Class for locomotive which drive a train.
 */
package kz.aitu.oop.practice.practice2.domain;

import kz.aitu.oop.practice.practice2.interfaces.Engine;
import kz.aitu.oop.practice.practice2.interfaces.Info;

public class Locomotive implements Engine, Info {
    private Color color; // color of locomotive
    private int maxSpeed;// maximum speed of locomotive

    public Locomotive(int maxSpeed){
        setColor(Color.NOT_COLORED);
        setMaxSpeed(maxSpeed);
    }

    public Locomotive(Color color, int maxSpeed) {
        setColor(color);
        setMaxSpeed(maxSpeed);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public void drive() {
        System.out.println("Locomotive is driving.");
    }

    @Override
    public void info() {
        System.out.println(color+" colored locomotive.");
    }
}
