/**
 * MedicalCar class.
 * It is a car for patients.
 */
package kz.aitu.oop.practice.practice2.domain.carTypes;

import kz.aitu.oop.practice.practice2.domain.Car;
import kz.aitu.oop.practice.practice2.domain.Color;

public class MedicalCar extends Car {

    public MedicalCar() {
    }

    public MedicalCar(int passengers) {
        super(passengers);
    }

    public MedicalCar(int passengers, Color color) {
        super(passengers, color);
    }

    // function for call all medics in current car to treat patients
    public void treat(){
        System.out.println("Patients are treated.");
    }

    @Override
    public void info() {
        System.out.println("This is a medical car which carries patients. It is designed for "+getPassengers()+" patients.");
    }
}
