/**
 * Engine which makes locomotive to drive.
 */
package kz.aitu.oop.practice.practice2.interfaces;

public interface Engine {
    void drive();
}
