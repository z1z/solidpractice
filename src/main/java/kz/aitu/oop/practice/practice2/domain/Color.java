package kz.aitu.oop.practice.practice2.domain;

public enum Color {
    NOT_COLORED("Not colored"),WHITE("White"), BLUE("Blue"), RED("Red"),
    YELLOW("Yellow"), GREEN("Green"), BLACK("Black");

    private String colorName;

    Color(String colorName){
        setColorName(colorName);
    }

    public String getColorName() {
        return colorName;
    }

    private void setColorName(String colorName) {
        this.colorName = colorName;
    }
}
