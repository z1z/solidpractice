package kz.aitu.oop.practice.practice2;

import kz.aitu.oop.practice.practice2.domain.Color;
import kz.aitu.oop.practice.practice2.domain.Locomotive;
import kz.aitu.oop.practice.practice2.domain.Train;
import kz.aitu.oop.practice.practice2.domain.carTypes.EconomClassCar;
import kz.aitu.oop.practice.practice2.domain.carTypes.MedicalCar;
import kz.aitu.oop.practice.practice2.domain.carTypes.RestaurantCar;
import kz.aitu.oop.practice.practice2.domain.carTypes.SeatedCar;

public class Main {

    public static void main(String[] args) {

        Train train = new Train(new Locomotive(120));

        train.hitch(new SeatedCar(50, Color.BLUE));
        train.hitch(new EconomClassCar(30, Color.YELLOW));
        train.hitch(new MedicalCar(40, Color.RED));
        train.hitch(new RestaurantCar(60, Color.WHITE));

        train.getCars().forEach(c->c.info());
        train.uncouple(2);
        train.getCars().forEach(c->c.info());

        System.out.println("Total speed of train: "+train.getPossibleSpeed());
        System.out.println("Maximum passengers: "+train.getTotalPassengers());
        train.drive();

    }
}
