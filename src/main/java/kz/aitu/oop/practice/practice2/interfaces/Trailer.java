/**
 * interface for hitch(add) and uncouple(remove) cars from train.
 */
package kz.aitu.oop.practice.practice2.interfaces;

import kz.aitu.oop.practice.practice2.domain.Car;

public interface Trailer {
    void hitch(Car car);
    void uncouple(int position);
}
