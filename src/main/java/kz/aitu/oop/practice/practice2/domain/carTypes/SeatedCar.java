/**
 * SeatedCar class.
 * It is a car where passengers can only seat.
 */
package kz.aitu.oop.practice.practice2.domain.carTypes;

import kz.aitu.oop.practice.practice2.domain.Car;
import kz.aitu.oop.practice.practice2.domain.Color;

public class SeatedCar extends Car {

    public SeatedCar(){
        super();
    }

    public SeatedCar(int passengers){
        super(passengers);
    }

    public SeatedCar(int passengers, Color color){
        super(passengers, color);
    }

    @Override
    public void info() {
        System.out.println("It is a seated car. It has "+getPassengers()+" seats.");
    }
}
