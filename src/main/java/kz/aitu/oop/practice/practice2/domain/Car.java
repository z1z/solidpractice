/**
 * Abstract class Car.
 * It implements Info, but doesn't describe it. Because,it is for concrete classes, which extends Car.
 */
package kz.aitu.oop.practice.practice2.domain;

import kz.aitu.oop.practice.practice2.interfaces.Info;

public abstract class Car implements Info {
    private int passengers; // total number of passengers who can be in current car
    private Color color;    // color of car

    public Car(){
        setColor(Color.NOT_COLORED);
    }

    public Car(int passengers) {
        this();
        setPassengers(passengers);
    }

    public Car(int passengers, Color color){
        setPassengers(passengers);
        setColor(color);
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
