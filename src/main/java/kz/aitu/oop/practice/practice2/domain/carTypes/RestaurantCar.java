/**
 * It is class for cars with restaurants.
 */
package kz.aitu.oop.practice.practice2.domain.carTypes;

import kz.aitu.oop.practice.practice2.domain.Car;
import kz.aitu.oop.practice.practice2.domain.Color;

public class RestaurantCar extends Car {

    public RestaurantCar() {
    }

    public RestaurantCar(int passengers) {
        super(passengers);
    }

    public RestaurantCar(int passengers, Color color) {
        super(passengers, color);
    }

    // Make feast for all passengers in this car.
    public void feast(){
        System.out.println("Making a feast!");
    }

    @Override
    public void info() {
        System.out.println("It is restaurant car. It is designed for "+getPassengers()+" passengers.");
    }
}
